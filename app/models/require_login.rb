module RequireLogin
  private

  def require_login
    unless logged_in?
      flash[:error] = "Please sign in first."
      redirect_to login_path
    end
  end
end
