Rails.application.routes.draw do
  root 'site/home#index'

  scope module: 'site' do
    get '/home', to: 'home#index'
  end

  scope module: 'backend' do
    get 'dashboard', to: 'dashboard#index'
    resources :users
    resources :episodes
    resources :sponsors
  end

  get '/login', to: 'backend/sessions#new'
  post '/login', to: 'backend/sessions#create'
  delete '/logout', to: 'backend/sessions#destroy'
end
