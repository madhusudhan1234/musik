class Backend::SponsorsController < ApplicationController
  include RequireLogin

  before_action :require_login
  layout "backend/layouts/application"

  def index
    @sponsors = Sponsor.all

    render 'index'
  end

  def new
    @sponsor = Sponsor.new

    render 'new'
  end

  def create
    @sponsor = Sponsor.new(sponsor_params)

    if @sponsor.save
      flash[:notice] = 'Sponsor has been created ! Congrats'
      redirect_to @sponsor
    else
      flash[:alert] = 'Sponsor has not been created'
      render 'new'
    end
  end

  def edit
    @sponsor = Sponsor.find(params[:id])

    render 'edit'
  end

  def update
    @sponsor = Sponsor.find(params[:id])

    if @sponsor.update(sponsor_params)
      redirect_to @sponsor
    else
      render 'edit'
    end
  end

  def show
    @sponsor = Sponsor.find(params[:id])

    render 'show'
  end

  def destroy
    @sponsor = Sponsor.find(params[:id])
    @sponsor.destroy

    redirect_to sponsors_path
  end

  private
  def sponsor_params
    params.require(:sponsor).permit(:name, :link, :description, :logo_link)
  end

end