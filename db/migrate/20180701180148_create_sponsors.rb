class CreateSponsors < ActiveRecord::Migration[5.1]
  def change
    create_table :sponsors do |t|
      t.string :name
      t.string :link
      t.text :description
      t.string :logo_link

      t.timestamps
    end
  end
end
