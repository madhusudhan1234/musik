class Backend::EpisodesController < ApplicationController
  include RequireLogin

  before_action :require_login
  layout "backend/layouts/application"

  def index
    @episodes = Episode.all

    render 'index'
  end

  def new
    @episode = Episode.new

    render 'new'
  end

  def create
    @episode = Episode.new(episode_params)
    @episode['user_id'] = 1

    if @episode.save
      flash[:notice] = "Episode has been created."
      redirect_to @episode
    else
      flash.now[:alert] = "Episode has not been created."
      render 'new'
    end
  end

  def edit
    @episode = Episode.find(params[:id])

    render 'edit'
  end

  def show
    @episode = Episode.find(params[:id])

    render 'show'
  end

  def update
    @episode = Episode.find(params[:id])

    if @episode.update(episode_params)
      redirect_to @episode
    else
      render 'edit'
    end
  end

  def destroy
    @episode = Episode.find(params[:id])
    abort @episode.inspect
    @episode.destroy

    redirect_to episodes_path
  end

  private
  def episode_params
    params.require(:episode).permit(:title, :description, :audio_link, :links)
  end
end