class User < ApplicationRecord
  attr_accessor :remember_token
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: { minimum: 2 }
  validates :email, presence: true, email:true, length: { minimum: 2 }
  validates :password, :presence => true, :length => {:within => 6..40}
  validates :twitter_username, presence: true, length: {minimum: 3}
  validates :github_username, presence: true, length: {minimum: 3}
  has_secure_password

  class << self
    def digest(string)
      cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost

      BCrypt::Password.create(string, cost: cost);
    end

    def new_token
      SecureRandom.urlsafe_base64
    end
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticate?(remember_token)
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_token, nil)
  end
end