class CreateJoinTableEpisodeSponsor < ActiveRecord::Migration[5.1]
  def change
    create_join_table :episodes, :sponsors do |t|
      # t.index [:episode_id, :sponsor_id]
      # t.index [:sponsor_id, :episode_id]
    end
  end
end
