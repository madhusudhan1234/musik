require "rails_helper"

RSpec.feature "Users can create new users" do
  before do
    visit "/users"

    click_link "Create User"
  end

  scenario "when providing invalid attributes" do
    click_button "Create User"

    expect(page).to have_content "User has not been created."
    expect(page).to have_content "Name can't be blank"
  end

  scenario "with valid attributes" do
    fill_in "user_name", with: "Madhu Sudhan Subedi"
    fill_in "user_email", with: "madhusudhansubedi4@gmail.com"
    fill_in "user_twitter_username", with: "madhusudhan12"
    fill_in "user_github_username", with: "madhusudhan"
    fill_in "user_password", with: "123445"
    click_button "Create User"

    expect(page).to have_content "User has been created."
  end
end