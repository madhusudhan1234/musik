import './scripts/masonry';
import './scripts/popover';
import './scripts/scrollbar';
import './scripts/search';
import './scripts/sidebar';
import './scripts/skycons';
import './scripts/chat';
import './scripts/email';
import './scripts/fullcalendar';
import './scripts/googleMaps';
import './scripts/utils';

class Confirm {
  constructor(el) {
    this.message = el.getAttribute('data-confirm');
    if (this.message) {
      el.form.addEventListener('submit', this.confirm.bind(this));
    } else {
      console && console.warn('No value specified in `data-confirm`', el);
    }
  }

  confirm(e) {
    if (!window.confirm(this.message)) {
      e.preventDefault();
    }
  }
}

window.onload = function() {
  Array.from(document.querySelectorAll('[data-confirm]')).forEach((el) => {
    new Confirm(el);
  });
};

