class Backend::DashboardController < ApplicationController
  include RequireLogin

  before_action :require_login
  layout 'backend/layouts/application'

  def index
    render 'index'
  end

end